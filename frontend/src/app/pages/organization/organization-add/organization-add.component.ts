import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { OrganizationService } from 'src/app/services/organization.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-organization-add',
  templateUrl: './organization-add.component.html',
  styleUrls: ['./organization-add.component.css']
})
export class OrganizationAddComponent implements OnInit {
  constructor(
    private router: Router,
    private organizationService: OrganizationService,
    private formBuilder: FormBuilder
  ) { }
  
  //get first input using viewchild
  @ViewChild("focusInput") focusInput: ElementRef;

  //form group init 
  organizationForm: FormGroup;

  //submitted flag
  submitted = false;

  //info success message
  message: string;
  
  ngOnInit(): void {
    this.createForm();
    
  }
  setFocus(): void {
    this.focusInput.nativeElement.focus();
  }
  createForm(): void {
    this.organizationForm = this.formBuilder.group({
      //form controls
      name: ['', Validators.required],
      owner: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
    })
  }

  //getter for form controls
  get f() {
    return this.organizationForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid do nothing
    if(this.organizationForm.invalid) {
      //if form is invalid, set focus to first element
      this.setFocus();
      return;
    }

    this.organizationService
    .create(this.organizationForm.value)
    .subscribe(() => {
      this.message = "✅  Successfully created new organization.";
      //hide message after a short time
      setTimeout(() => {
        this.message = null;
      }, 1000);
      //reinit the form
      this.createForm();
      this.setFocus();
      this.submitted = false;
      // this.goBack();
    })
  }
  goBack(): void {
    this.router.navigateByUrl('/organizations');
  }

}
