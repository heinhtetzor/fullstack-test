import { Component, OnInit } from '@angular/core';
import { OrganizationService } from 'src/app/services/organization.service';
import { Organization } from 'src/app/models/organization.mode';

@Component({
  selector: 'app-organization-list',
  templateUrl: './organization-list.component.html',
  styleUrls: ['./organization-list.component.css']
})
export class OrganizationListComponent implements OnInit {

  //local varaibles binded to view
  organizations: Organization[];

  //to determine whether to render table
  isEmpty: boolean;
  constructor(private organizationService: OrganizationService) { }

  ngOnInit(): void {
    this.fetchData();
  }
  fetchData(): void {
    this.organizationService.getAll().subscribe((organizations:[Organization]) => {
      this.organizations = organizations;
      organizations.length > 0 ? this.isEmpty = false : this.isEmpty = true;
    })
  }
  delete(id: number, name: string): void {
    if(confirm(`Are you sure you want to delete ${name}? \nAll departments and employees under this organization will also be deleted.`)) {
      this.organizationService.delete(id)
      .subscribe(res => {
        if(res) {
          this.fetchData();
        }
      })
    }
  }

}
