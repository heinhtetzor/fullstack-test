import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { OrganizationService } from 'src/app/services/organization.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Organization } from 'src/app/models/organization.mode';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-organization-edit',
  templateUrl: './organization-edit.component.html',
  styleUrls: ['./organization-edit.component.css']
})
export class OrganizationEditComponent implements OnInit {

  //store organization id
  organizationId: number;

  //organization name to display at header
  organizationName: string;

  //form group init 
  organizationForm: FormGroup;
  
  //submitted flag
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private organizationService: OrganizationService
  ) { }
  
  get f() {
    return this.organizationForm.controls;
  }

  ngOnInit(): void {
    //get the parameter id first
    //and send request by using getOne
    this.route.params
    .pipe(
      switchMap((params: Params) => {
        this.organizationId = params.id;
        return this.organizationService.getOne(this.organizationId);
      })
    )
    .subscribe((organization: Organization) => {
      this.organizationName = organization.name;
        this.organizationForm = this.formBuilder.group({
          //form controls
          name: [organization.name, Validators.required],
          owner: [organization.owner, Validators.required],
          address: [organization.address, Validators.required],
          city: [organization.city, Validators.required],
          state: [organization.state, Validators.required],
          country: [organization.country, Validators.required],
      
        })
      })
  }

  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid, do nothing
    if(this.organizationForm.invalid) {
      return;
    }
    this.organizationService
    .update(this.organizationId, this.organizationForm.value)
    .subscribe(() => {
      this.goBack();
    })
  }
  goBack(): void {
    this.router.navigateByUrl('/organizations');
  }

}
