import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  //form init
  loginForm: FormGroup;
  //login error
  loginError: string;
  //submit flag
  submitted = false;

  ngOnInit(): void {
    //building form
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  //getter for form controls
  get f() {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    //if the form is invalid, do nothing
    if(this.loginForm.invalid) {
      return;
    }
    this.authService
    .login(this.loginForm.value)
    .subscribe(res => {
      //storing credientials info in local storage
      const name = jwt_decode(res).firstName;
      localStorage.setItem('token', res);
      localStorage.setItem('name', name);
      this.router.navigateByUrl('/');
    }, 
    err => {
      this.loginError = err.error;
    })
  }

  goToRegister() {
    this.router.navigateByUrl('/register');
  }
}
