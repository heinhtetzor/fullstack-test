import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }
  
  //init form group
  registerForm: FormGroup;

  //submit flag
  submitted: boolean = false;

  //to show success message
  isSuccessful: boolean = false;

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validators: this.mustMatch.bind(this)
    })
  }
  
  get f() {
    return this.registerForm.controls;
  }

  //helper function to check passwords match
  mustMatch(loginForm: FormGroup) {
    const password = loginForm.get('password');
    const confirmPassword = loginForm.get('confirmPassword');
    //if there is no password values, exit the function
    if(!password.value && !confirmPassword.value) return;

    if(password.value !== confirmPassword.value) {
      confirmPassword.setErrors({ confirmValidator: true })
    }
    else {
      confirmPassword.setErrors(null);
    }
  }

  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid do nothing
    if(this.registerForm.invalid) {
      return;
    }
    this.authService
    .register(this.registerForm.value)
    .subscribe(res => {
      if(res) {
        this.isSuccessful = true;
      }
    })
  }

  goToLogin(): void {
    this.router.navigateByUrl('/login');
  }

}
