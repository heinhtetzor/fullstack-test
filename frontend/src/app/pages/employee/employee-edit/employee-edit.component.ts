import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { OrganizationService } from 'src/app/services/organization.service';
import { DepartmentService } from 'src/app/services/department.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Organization } from 'src/app/models/organization.mode';
import { Department } from 'src/app/models/department.model';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
  	private router: Router,
  	private employeeService: EmployeeService,
  	private departmentService: DepartmentService,
  	private formBuilder: FormBuilder,
  	private organizationService: OrganizationService
  	) { }
  //employee first name to display in view
  employeeFirstName: string;
  employeeId: number;
  //form group init 
  employeeForm: FormGroup;
  //submitted flag
  submitted = false;
  //organizations for Organization select box
  organizations: Organization[];
  //departments for Department select box
  departments: Department[];

  get f() {
    return this.employeeForm.controls;
  }

  ngOnInit(): void {

    this.route.params.subscribe((params:Params) => {
      //get the parameter id first
      //and send request by using getOne
      this.employeeId = params.id;
      this.employeeService.getOne(this.employeeId).subscribe((employee: Employee) => {
        //assign first name to show in header
        this.employeeFirstName = employee.firstName;
        //call department service to fill department select box
        this.organizationService.getDepartments(employee.Department.OrganizationId)
        .subscribe((departments: Department[]) => {
          this.departments = departments;
        })
        this.employeeForm = this.formBuilder.group({
          //form controls
          organizationName: [employee.Department.Organization.name, Validators.required],
          firstName: [employee.firstName, Validators.required],
          lastName: [employee.lastName, Validators.required],
          DepartmentId: [employee.DepartmentId, Validators.required],
          dob: [employee.dob, Validators.required],
          workTitle: [employee.workTitle, Validators.required],
          totalExperience: [employee.totalExperience, Validators.required],
        })
      })
    })
  }
  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid, do nothing
    if(this.employeeForm.invalid) {
      return;
    }
    this.employeeService
    .update(this.employeeId, this.employeeForm.value)
    .subscribe(() => {
      this.goBack();
    })
  }
  goBack(): void {
    this.router.navigateByUrl('/employees');
  }

}
