import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  //local varaibles binded to view
  employees: Employee[];
  isEmpty: boolean;

  constructor(
  	private employeeService: EmployeeService,
  	) { }

  ngOnInit(): void {
    this.fetchData();
  }
  fetchData(): void {
    this.employeeService.getAll()
    .subscribe((employees:[Employee]) => {
      this.employees = employees;
      //setting isEmpty boolean to show info message in view
      employees.length > 0 ? this.isEmpty = false : this.isEmpty = true;
    })
  }
  delete(id: number, name: string): void {
    if(confirm(`Are you sure you want to delete ${name}?`)) {
      this.employeeService.delete(id)
      .subscribe(res => {
        if(res) {
          this.fetchData();
        }
      })
    }
  }
}
