import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { OrganizationService } from 'src/app/services/organization.service';
import { DepartmentService } from 'src/app/services/department.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Organization } from 'src/app/models/organization.mode';
import { Department } from 'src/app/models/department.model';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  constructor(
  	private router: Router,
  	private employeeService: EmployeeService,
  	private formBuilder: FormBuilder,
  	private organizationService: OrganizationService
    ) { }
    
  //form group init 
  employeeForm: FormGroup;

  //flash message
  message: string;

  //submitted flag
  submitted = false;

  //native element reference to set focus
  @ViewChild('focusInput') focusInput: ElementRef;

  //organizations for Organization select box
  organizations: Organization[];

  //departments for Department select box
  departments: Department[];

  //selected oranization id to populate departments select box
  organizationId: string;

  ngOnInit(): void {
    //call organization service to fetch
    this.organizationService.getAll()
    .subscribe((organizations: Organization[]) => {
      this.organizations = organizations;
    })

    this.createForm();

    this.employeeForm.get('OrganizationId').valueChanges
    .pipe(
      flatMap(oId => {
        return this.organizationService.getDepartments(oId);
      })
    )
    .subscribe(departments => {
      this.departments =departments;
    })
  }
  
  createForm(): void {
    this.employeeForm = this.formBuilder.group({
      //form controls
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      OrganizationId: [''],
      DepartmentId: ['', Validators.required],
      dob: ['', Validators.required],
      workTitle: ['', Validators.required],
      totalExperience: ['', Validators.required],
    })
  }

  setFocus(): void {
    this.focusInput.nativeElement.focus();
  }

  get f() {
  	return this.employeeForm.controls;
  }
  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid do nothing
    if(this.employeeForm.invalid) {
      //if the form is invalid, set focus to first element
      this.setFocus();
      return;
    }
    this.employeeService
    .create(this.employeeForm.value)
    .subscribe(() => {
      this.message = "✅  Successfully created new employee.";
      //hide message after a short time
      setTimeout(() => {
        this.message = null;
      }, 1000);
      this.createForm();
      this.setFocus();
      this.submitted = false;
    })
  }
  goBack(): void {
  	this.router.navigateByUrl('/employees');
  }

}
