import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { FormControl } from '@angular/forms';
import { Chart } from 'chart.js';
import * as moment from 'moment';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements AfterViewInit {
  //select canvas from html
  @ViewChild('chartCanvas') chartCanvas;
  //static months array
  static MONTHS = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  //store chart instance
  chart: any;

  //chart options
  //chart title
  chartTitle: string;

  //labels x axis
  chartLabels: number[] | string[];

  //flag to show months labels whether to generate using months arrays
  is1Y: boolean;

  //organizations array for chart
  ogs: number[];

  //departments array for chart
  dps: number[];
  
  //employess array for chart
  emps: number[];
  
  //dates are defaulted to today dates
  dateFrom = new FormControl(new Date());
  dateTo = new FormControl(new Date());

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngAfterViewInit(): void {
    this.get1MData();
  }
  initChart():void {
    //destroy the pre-existing chart to draw another chart
    if(this.chart) {
      this.chart.destroy();
    }
    //initiate chart using nativeElement
    this.chart = new Chart(this.chartCanvas.nativeElement, {
      responsive:true,
      maintainAspectRatio: false,
      type: 'bar',
      data: {
        //dates or months
        labels: this.chartLabels,
        datasets: [
          //Organization Dataset
            {
            label: 'Organizations',
            data: this.ogs,
            backgroundColor: this.generateColors(this.dps.length, 'blue'),
            borderWidth: 1
          },
          //Department Datasets
          {
            label: 'Departments',
            data: this.dps,
            backgroundColor: this.generateColors(this.dps.length, 'brown'),
            borderWidth: 1
          },
          //Employee Datasets
          {
            label: 'Employees',
            data: this.emps,
            backgroundColor: this.generateColors(this.dps.length, 'green'),
            borderWidth: 1,
          },
        ]
      },
      options: {
        title: {
          display: true,
          text: this.chartTitle
        },
        scales: {
          
          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: 1,

            }
          }]
        }
      }
    });
  }
  get1MData(): void {
    this.dashboardService.get1MChartData(this.dateFrom.value, this.dateTo.value)
    .subscribe((res: any) => {
      this.is1Y = false;
      this.chartLabels = this.generateLabels(res.labels);
      this.chartTitle = `${ChartComponent.MONTHS[res.selectedMonth]} - ${res.selectedYear}`;
      this.ogs = res.ogs;
      this.dps = res.dps;
      this.emps = res.emps;
      this.initChart();
    })
  }
  get1YData(): void {
    this.dashboardService.get1YChartData(this.dateFrom.value, this.dateTo.value)
    .subscribe((res: any) => {
      this.is1Y = true;
      this.chartLabels = this.generateLabels(res.labels);
      this.chartTitle = `Year ${res.selectedYear}`;
      this.ogs = res.ogs;
      this.dps = res.dps;
      this.emps = res.emps;
      this.initChart();
    })
  }
  generateLabels(n): number[] | string[] {
    //if 1y, just show the array of months' labels
    if(this.is1Y) {
      return ChartComponent.MONTHS;
    }
    // else generate number of days based on selected month
    let _arr = [];
    for(let i = 0; i < n; i++) {
      _arr.push(i+1);
    }
    return _arr;
  }
  generateColors(n: number, color: string): string[] {
    let _arr = [];
    for(let i = 0; i < n; i++) {
      _arr.push(color);
    }
    return _arr;
  }

}
