import { Component, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //declaring each instance varaibles
  ogsCount: number;
  dpsCount: number;
  empsCount: number;

  constructor(
    private dashboardService: DashboardService
  ) { }


  ngOnInit(): void {
    //getting organization count
    this.dashboardService.getOgsCount()
    .subscribe((count: number) => {
      this.ogsCount = count;
    })
    //getting department count
    this.dashboardService.getDpsCount()
    .subscribe((count: number) => {
      this.dpsCount = count;
    })
    //getting employee count
    this.dashboardService.getEmpsCount()
    .subscribe((count: number) => {
      this.empsCount = count;
    })
  }

}
