import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DepartmentService } from 'src/app/services/department.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrganizationService } from 'src/app/services/organization.service';
import { Organization } from 'src/app/models/organization.mode';

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {

  constructor(
    private router: Router,
    private departmentService: DepartmentService,
    private organizationService: OrganizationService,
    private formBuilder: FormBuilder
  ) { }

  //form group init 
  departmentForm: FormGroup;

  //get focusInput
  @ViewChild('focusInput') focusInput: ElementRef;

  //submitted flag
  submitted = false;

  //flash message
  message: string;
  
  //organizations for Organization select box
  organizations: Organization[];

  ngOnInit(): void {
    //call organization service to fetch
    this.organizationService.getAll()
    .subscribe((organizations: Organization[]) => {
      this.organizations = organizations;
    })
    this.createForm();
  }
  createForm(): void {
    this.departmentForm = this.formBuilder.group({
      //form controls
      OrganizationId: ['', Validators.required],
      owner: ['', Validators.required],
      description: ['', Validators.required],
      workingtime: ['', Validators.required],
      workingdays: ['', Validators.required]
    })
  }
  setFocus(): void {
    this.focusInput.nativeElement.focus();
  }
  get f() {
    return this.departmentForm.controls;
  }
  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid do nothing
    if(this.departmentForm.invalid) {
      this.setFocus();
      return;
    }
    this.departmentService
    .create(this.departmentForm.value)
    .subscribe(() => {
      this.message = "✅  Successfully created new department.";
      //hide message after a short time
      setTimeout(() => {
        this.message = null;
      }, 1000);
      this.submitted = false;
      this.departmentForm.reset();
      this.setFocus();
    })
  }
  goBack(): void {
    this.router.navigateByUrl('/departments');
  }
}
