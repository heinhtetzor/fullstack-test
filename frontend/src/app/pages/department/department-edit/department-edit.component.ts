import { Component, OnInit, ɵConsole } from '@angular/core';
import { DepartmentService } from 'src/app/services/department.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params, ParamMap } from '@angular/router';
import { Department } from 'src/app/models/department.model';
import { Organization } from 'src/app/models/organization.mode';
import { OrganizationService } from 'src/app/services/organization.service';

import { switchMap, flatMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {

  //store department id
  departmentId: number;

  //department name to display at header
  departmentDesc: string;

  //form group init 
  departmentForm: FormGroup;

  //submitted flag
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private departmentService: DepartmentService,
    private organizationService: OrganizationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  
  get f() {
    return this.departmentForm.controls;
  }

  organizations: Organization[];

  ngOnInit(): void {
    //call organization service to fetch
    this.organizationService.getAll()
    .subscribe((organizations: Organization[]) => {
      this.organizations = organizations;
    })
    //get the parameter id first
    //and send request by using getOne
    this.route.params
    .pipe(
      switchMap((params: Params) => {
        this.departmentId = params.id;
        return this.departmentService.getOne(this.departmentId);
      })
    )
    .subscribe((department: Department) => {
      this.departmentDesc = department.description;
      this.departmentForm = this.formBuilder.group({
        //form controls
        organizationName: [department.Organization.name, Validators.required],
        owner: [department.owner, Validators.required],
        description: [department.description, Validators.required],
        workingtime: [department.workingtime, Validators.required],
        workingdays: [department.workingdays, Validators.required],
      })
    })
  }
  onSubmit(): void {
    this.submitted = true;
    //if the form is not valid, do nothing
    if(this.departmentForm.invalid) {
      return;
    }
    this.departmentService
    .update(this.departmentId, this.departmentForm.value)
    .subscribe(() => {
      this.goBack();
    })
  }
  goBack(): void {
    this.router.navigateByUrl('/departments');
  }

}
