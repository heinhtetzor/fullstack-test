import { Component, OnInit } from '@angular/core';
import { Department } from 'src/app/models/department.model';
import { DepartmentService } from 'src/app/services/department.service';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  //local varaibles binded to view
  departments: Department[];

  //to determine whether to show table or not
  isEmpty: boolean;

  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData(): void {
    this.departmentService.getAll().subscribe((departments:[Department]) => {
      this.departments = departments;
      departments.length > 0 ? this.isEmpty = false : this.isEmpty = true;
    })
  }
  delete(id: number, name: string): void {
    if(confirm(`Are you sure you want to delete ${name}? \nAll employees under this department will also be deleted.`)) {
      this.departmentService.delete(id)
      .subscribe(res => {
        if(res) {
          this.fetchData();
        }
      })
    }
  }

}
