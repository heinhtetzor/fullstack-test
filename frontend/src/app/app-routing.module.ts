import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationListComponent } from './pages/organization/organization-list/organization-list.component';
import { OrganizationAddComponent } from './pages/organization/organization-add/organization-add.component';
import { OrganizationEditComponent } from './pages/organization/organization-edit/organization-edit.component';
import { DashboardComponent } from './pages/dashboard/dashboard/dashboard.component';
import { DepartmentListComponent } from './pages/department/department-list/department-list.component';
import { DepartmentAddComponent } from './pages/department/department-add/department-add.component';
import { DepartmentEditComponent } from './pages/department/department-edit/department-edit.component';
import { EmployeeListComponent } from './pages/employee/employee-list/employee-list.component';
import { EmployeeAddComponent } from './pages/employee/employee-add/employee-add.component';
import { EmployeeEditComponent } from './pages/employee/employee-edit/employee-edit.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {
    path: '', 
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'organizations', 
    component: OrganizationListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'organizations/add', 
    component: OrganizationAddComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'organizations/edit/:id', 
    component: OrganizationEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'departments', 
    component: DepartmentListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'departments/add', 
    component: DepartmentAddComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'departments/edit/:id', 
    component: DepartmentEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees', 
    component: EmployeeListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees/add', 
    component: EmployeeAddComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees/edit/:id', 
    component: EmployeeEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login', 
    component: LoginComponent
  },
  {
    path: 'register', 
    component: RegisterComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
