import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute, RoutesRecognized, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.css']
})
export class SidebarMenuComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
    ) { }
  //isLoggedIn to determine what to show on sidebar
  showMenus: boolean;

  name: string;

  ngOnInit(): void {
    this.router.events.subscribe((r: any) => {
      if(r instanceof NavigationEnd) {
        if(r.url === '/login' || r.url === '/register') {
          this.showMenus = false;
          this.setUser();
        }
        else {
          this.showMenus = true;
          this.setUser();
        }
      }
    })
    //get name from authservice and set to local varaible
  }
  
  setUser(): void {
    this.name = this.authService.getUser();
  }

  logout(): void {
    if (this.authService.isLoggedIn()) {
      if(confirm('Are you sure you want to logout?')) {
        this.authService.logout();
        this.router.navigateByUrl('/login');
      }
    }
    else {
      console.log('you have not logged in.');
    }
  }

}
