import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-button',
  templateUrl: './edit-button.component.html',
  styleUrls: ['./edit-button.component.css']
})
export class EditButtonComponent implements OnInit {
  @Input() domain: string;
  @Input() id: number;
  constructor() { }

  ngOnInit(): void {
  }

}
