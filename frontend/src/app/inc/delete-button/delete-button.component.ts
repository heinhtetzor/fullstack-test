import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from 'angular-modal-library';
import { OrganizationService } from 'src/app/services/organization.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { DepartmentService } from 'src/app/services/department.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.css']
})
export class DeleteButtonComponent implements OnInit {
  @Input() domain: string;
  @Input() deleteId: number;

  constructor(
    private router: Router,
    private organizationService: OrganizationService,
    private employeeService: EmployeeService,
    private departmentService: DepartmentService
    ) { }

  ngOnInit(): void {
  }

  delete(): void {
    
    if(this.domain === 'organizations') {
      this.organizationService
      .delete(this.deleteId)
      .subscribe(isDeleted => {
        if(isDeleted) {
          this.router.navigateByUrl('/', { skipLocationChange: true });
          this.router.navigate(['/organizations']);
        }
      })
    }
    else if(this.domain === 'employees') {

    }
    else if(this.domain === 'departments') {

    }
  }

}
