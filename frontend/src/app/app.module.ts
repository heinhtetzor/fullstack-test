import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrganizationListComponent } from './pages/organization/organization-list/organization-list.component';
import { OrganizationAddComponent } from './pages/organization/organization-add/organization-add.component';
import { OrganizationEditComponent } from './pages/organization/organization-edit/organization-edit.component';
import { DashboardComponent } from './pages/dashboard/dashboard/dashboard.component';
import { PageHeaderComponent } from './inc/page-header/page-header.component';
import { AddButtonComponent } from './inc/add-button/add-button.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EditButtonComponent } from './inc/edit-button/edit-button.component';
import { DeleteButtonComponent } from './inc/delete-button/delete-button.component';
import { DepartmentAddComponent } from './pages/department/department-add/department-add.component';
import { DepartmentEditComponent } from './pages/department/department-edit/department-edit.component';
import { DepartmentListComponent } from './pages/department/department-list/department-list.component';
import { EmployeeAddComponent } from './pages/employee/employee-add/employee-add.component';
import { EmployeeEditComponent } from './pages/employee/employee-edit/employee-edit.component';
import { EmployeeListComponent } from './pages/employee/employee-list/employee-list.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { SidebarMenuComponent } from './inc/sidebar-menu/sidebar-menu.component';
import { ChartComponent } from './pages/dashboard/chart/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    OrganizationListComponent,
    OrganizationAddComponent,
    OrganizationEditComponent,
    DashboardComponent,
    PageHeaderComponent,
    AddButtonComponent,
    EditButtonComponent,
    DeleteButtonComponent,
    DepartmentAddComponent,
    DepartmentEditComponent,
    DepartmentListComponent,
    EmployeeAddComponent,
    EmployeeEditComponent,
    EmployeeListComponent,
    LoginComponent,
    RegisterComponent,
    SidebarMenuComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    //setting up interceptors
    { provide: HTTP_INTERCEPTORS, 
      useClass: TokenInterceptor, 
      multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
