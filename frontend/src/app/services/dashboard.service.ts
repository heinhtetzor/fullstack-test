import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private webReq: WebRequestService
  ) { }

  //getting tiles data
  //organizations
  getOgsCount(): Observable<any> {
    return this.webReq.get('dashboard/organizationsCount');
  }
  //departments
  getDpsCount(): Observable<any> {
    return this.webReq.get('dashboard/departmentsCount');
  }
  //employees
  getEmpsCount(): Observable<any> {
    return this.webReq.get('dashboard/employeesCount');
  }

  //for charting monthly
  get1MChartData(dateFrom: Date, dateTo: Date): Observable<any> {
    return this.webReq.get(`dashboard/get1MChartData/${dateFrom}/${dateTo}`);
  }
  //for charting yearly
  get1YChartData(dateFrom: Date, dateTo: Date): Observable<any> {
    return this.webReq.get(`dashboard/get1YChartData/${dateFrom}/${dateTo}`);
  }

}
