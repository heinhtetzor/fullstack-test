import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private webReq: WebRequestService) { }

  //Post request to login
  login(loginForm: any): Observable<any> {
    return this.webReq.post('auth/login', loginForm)
  }

  //Post request to register
  register(user: User): Observable<any> {
    return this.webReq.post('auth/register', user)
  }

  //just removing saved items from localstorage
  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('name');
  }

  //get token from localstorage to decode and get data
  getToken(): string {
    return localStorage.getItem('token');
  }

  //get the firstName of user from localstorage
  getUser(): string {
    if(this.isLoggedIn()) {
      return localStorage.getItem('name').toUpperCase();
    }
    return;
  }

  //if the user logged in, or not by verifying jwt token
  isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    try {
      if(jwt_decode(token)) {
        return true;
      }
    }
    catch(err) {
      return false;
    }
  }
}
