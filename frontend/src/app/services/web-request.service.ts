import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

//Interface for web request services
//GET, POST, PUT, DELETE
export class WebRequestService {
  readonly URL;
  constructor(private http:HttpClient) { 
    this.URL = 'http://localhost:5000';
  }
  get(uri: string){
    return this.http.get(`${this.URL}/${uri}`);
  }
  post(uri: string, payload: Object){
    return this.http.post(`${this.URL}/${uri}`, payload);
  }
  put(uri: string, payload: Object){
    return this.http.put(`${this.URL}/${uri}`, payload);
  }
  delete(uri: string){
    return this.http.delete(`${this.URL}/${uri}`);
  }
}
