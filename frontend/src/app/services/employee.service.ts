import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private webReq: WebRequestService) { }

  create(employee: Employee): Observable<any> {
    return this.webReq.post('employees', employee)
  }
  getAll(): Observable<any> {
    return this.webReq.get('employees');
  }
  getOne(id: number): Observable<any> {
    return this.webReq.get(`employees/${id}`);
  }
  update(id: number, employee: Employee): Observable<any> {
    return this.webReq.put(`employees/${id}`, employee);
  }
  delete(id: number): Observable<any> {
    return this.webReq.delete(`employees/${id}`);
  }
}
