import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Department } from '../models/department.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private webReq: WebRequestService) { }

  create(department: Department): Observable<any> {
    return this.webReq.post('departments', department)
  }
  getAll(): Observable<any> {
    return this.webReq.get('departments');
  }
  getOne(id: number): Observable<any> {
    return this.webReq.get(`departments/${id}`);
  }
  update(id: number, department: Department): Observable<any> {
    return this.webReq.put(`departments/${id}`, department);
  }
  delete(id: number): Observable<any> {
    return this.webReq.delete(`departments/${id}`);
  }
}
