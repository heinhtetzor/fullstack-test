import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Organization } from '../models/organization.mode';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private webReq: WebRequestService) { }

  create(organization: Organization): Observable<any> {
    return this.webReq.post('organizations', organization)
  }
  getAll(): Observable<any> {
    return this.webReq.get('organizations');
  }
  getOne(id: number): Observable<any> {
    return this.webReq.get(`organizations/${id}`);
  }
  getDepartments(id: number): Observable<any> {
    return this.webReq.get(`organizations/${id}/departments`);
  }
  update(id: number, organization: Organization): Observable<any> {
    return this.webReq.put(`organizations/${id}`, organization);
  }
  delete(id: number): Observable<any> {
    return this.webReq.delete(`organizations/${id}`);
  }
}
