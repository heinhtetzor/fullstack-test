import { Department } from './department.model';

export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    dob: string;
    workTitle: string;
    totalExperience: string;
    DepartmentId: number;
    Department?: Department
}