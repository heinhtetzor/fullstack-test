import { Organization } from './organization.mode';

export class Department {
    id: number;
    owner: string;
    description: string;
    workingtime: string;
    workingdays: number;
    OrganizationId: number;
    Organization?: Organization;
}