export class Organization {
    id: number;
    name: string;
    owner: string;
    address: string;
    city: string;
    state: string;
    country: string;
    timestamps: string;
}