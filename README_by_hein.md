**Frontend**
- Global css styles are kept under /assets/global.css
- There are 6 main sections in frontend.
- guards - Authguard -To protect routes, handle unauthorised user redirection
- inc - Reusable component for simple presentational parts such as header, sidebar
- interceptors - Make authorised request by cloning the request with the verified token and send to backend
- models - DTOs object to encapsulate data to ensure consistency
- pages - all the page components that the application has, folder separated accordingly. 
- services - organisations, employees, departments, dashboard services and auth services, mostly used to fetch from Api

**Other highlights** 
- No css frameworks are used. Media queries are handwritten.
- Used momentjs for convenient date processing
- Used chartjs for charting
- Chart services and related API requests are written inside dashboard.service.ts.
- By default, chart will show data related to today’s date.
- jwt token is stored in localStorage

**Backend**
- used MySQL for database
- used sequelize for ORM
- used jsonwebtoken for jwt authentication
- entities definition are kept under /config/entities
- database connection settings are defined in /config/entities/db.config.js
- routes folder - handling all routes , all routes except login and register has authenticate middleware in between
- authenticate middleware is kept under /middlewares/authenticate

**Containerisation**
- There are 3 services
- Each of three folders in root folder has dockerfile 
- frontend-port = 4200
- backend-port = 5000
- mysql-port = 3306

run “docker-compose up —build -d” to install all dependencies and start the services.

http://localhost:4200
