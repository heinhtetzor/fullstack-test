const express = require('express');
const app = express();
const db = require('./config/db.config');
//import routes
const authRouter = require('./routes/auth.js');
const organizationRouter = require('./routes/organizations.js');
const departmentRouter = require('./routes/departments.js');
const employeeRouter = require('./routes/employees.js');
const dashboardRouter = require('./routes/dashboard.js');
//json body parser
app.use(express.json());

//cors middleware
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods" , "GET,POST,OPTIONS,DELETE,PUT,PATCH");
    next();
  });



//routes handling
app.use('/auth', authRouter);
app.use('/organizations', organizationRouter);
app.use('/departments', departmentRouter);
app.use('/employees', employeeRouter);
app.use('/dashboard', dashboardRouter)
app.use('/auth', authRouter);

//for testing purposes
app.get('/testdb', (req, res) => {
    db.authenticate()
    .then(() => {
        res.send('Successfully connected to database');
    })
    .catch(err => {
        res.send('trouble connecting to database', err);
    })
})

const PORT = 5000;

app.listen(PORT, () => console.log(`Server is listening at ${PORT}`));