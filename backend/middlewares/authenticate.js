const express = require('express');
const jwt = require('jsonwebtoken');
//import env file
const { PRIVATE_KEY } = require('../env');

module.exports = (req, res, next) => {
    //get the ess token from the client request header
    const token = req.header('Authorization');
    jwt.verify(token, PRIVATE_KEY, (err, decoded) => {
        if(err) {
            res.status(401)
            .send(err);
        }
        else {
            next();
        }
    })
}