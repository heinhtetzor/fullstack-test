const express = require('express');
const User = require('../config/entities/user');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const PRIVATE_KEY = '%$^*/:/><443%fgg#%$jjf';

//authentication routes
//login 
router.post('/login', async(req, res) => {
    //check if there is any user with requested email
    const user = await User.findOne({
        where: {
            email: req.body.email
        }
    })
    if(user) {
        bcrypt.compare(req.body.password, user.password, (err, result) => {
            if(result) {
                //sign with jwt
                jwt.sign({firstName: user.firstName}, PRIVATE_KEY, (err, token) => {
                    //create new token and send to client
                    if(!err) {
                        res.json(token);
                    }
                    else {
                        console.log(err);
                    }
                })
            }
            else {
                res.status(403)
                .send('Wrong Credentials');
            }
        })
    }
    else {
        res.status(404)
        .send('User Not Found');
    }
})

//new user registration
router.post('/register', (req, res) => {
    const saltRounds = 10;
    //seperate password and other data
    const { password } = req.body;
    const { firstName, lastName, email } = req.body;

    let userToBeSaved = {
        firstName,
        lastName,
        email
    };
    //hash the plain password
    bcrypt.hash(password, saltRounds, async(err, hash) => {
        //combine password with usersToBeSaved object to save to database
        userToBeSaved = {
            ...userToBeSaved,
            password: hash
        }
        //perform database operation
        try {
            const user = await User.create(userToBeSaved);
            res.json(user);
        } 
        catch (error) {
            res.status(500)
            .send(error.message);
        }
    })
})
//for testing purposes
router.delete('/delete', (req, res) => {
    User.destroy({
        where: {
            id: req.body.id
        }
    })
    .then(result => {
        res.json(result)
    })
})

module.exports = router;