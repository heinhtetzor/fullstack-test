const express = require('express');
const router = express.Router();

const Organization = require('../config/entities/organization.js');
const Department = require('../config/entities/department.js');
const authenticate = require('../middlewares/authenticate');

//get all organizations
router.get('/', authenticate, async(req, res) => {
    try {
        const result = await Organization.findAll();
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message)
    }
})

//get departments by given organization id
router.get('/:id/departments', authenticate, async(req, res) => {
    try {
        const result = await Department.findAll({
            where: {
                OrganizationId: req.params.id
            }
        });
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message);
    }    
})

//get record by ID
router.get('/:id', authenticate, async(req, res) => {
    try {
        const result = await Organization.findByPk(req.params.id);
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message)
    }
    
})

//create new organization record
router.post('/', authenticate, async(req, res) => {
    try {
        const result = await Organization.create(req.body);
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message)
    }
})

//update existing record
router.put('/:id', authenticate, async(req, res) => {
    try {
        const result = await Organization.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message)
    }
})

//delete existing record
router.delete('/:id', authenticate, async(req, res) => {
    try {
        const result = await Organization.destroy({
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message)
    }
})


module.exports = router;