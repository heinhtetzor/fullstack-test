const express = require('express');
const router = express.Router();
const Department = require('../config/entities/department.js');
const Organization = require('../config/entities/organization.js');
const authenticate = require('../middlewares/authenticate');
//get all departments
router.get('/', authenticate, async(req, res) => {
    try {
        const result = await Department.findAll({
            include: [{
                model: Organization
            }]
        });
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message);
    }
})

//get record by ID
router.get('/:id', authenticate, async(req, res) => {
    try {
        const result = await Department.findByPk(req.params.id, {
            include: [{
                model: Organization
            }]
        });
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message);
    }
    
})


//create new department record
router.post('/', authenticate, async(req, res) => {
    try {
        const result = await Department.create(req.body);
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})

//update existing record
router.put('/:id', authenticate, async(req, res) => {
    try {
        const result = await Department.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})

//delete existing record
router.delete('/:id', authenticate, async(req, res) => {
    try {
        const result = await Department.destroy({
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})


module.exports = router;