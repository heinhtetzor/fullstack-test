const express = require('express');
const router = express.Router();
const Employee = require('../config/entities/employee.js');
const Department = require('../config/entities/department.js');
const Organization = require('../config/entities/organization.js');
const authenticate = require('../middlewares/authenticate');

//get all employees
router.get('/', authenticate, async(req, res) => {
    try {
        const result = await Employee.findAll({
            include: [{
                model: Department,
                include: [{
                    model: Organization
                }]
            }]
        });
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message);
    }
})

//get record by ID
router.get('/:id', authenticate, async(req, res) => {
    try {
        const result = await Employee.findByPk(req.params.id, {
            //nested eager loading
            include: [{
                model: Department,
                include: [{
                    model: Organization
                }]
            }]
        });
        res.json(result);
    }
    catch (error) {
        res.status(404)
        .send(error.message);
    }
    
})

//create new employee record
router.post('/', authenticate, async(req, res) => {
    try {
        const result = await Employee.create(req.body);
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})

//update existing record
router.put('/:id', authenticate, async(req, res) => {
    try {
        const result = await Employee.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})

//delete existing record
router.delete('/:id', authenticate, async(req, res) => {
    try {
        const result = await Employee.destroy({
            where: {
                id: req.params.id
            }
        })
        res.json(result);
    } 
    catch (error) {
        res.status(500)
        .send(error.message);
    }
})


module.exports = router;