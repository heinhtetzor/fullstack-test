const express = require('express');
const authenticate = require('../middlewares/authenticate');
const Organization = require('../config/entities/organization');
const Department = require('../config/entities/department');
const Employee = require('../config/entities/employee');
const { Op, Sequelize } = require('sequelize');
const router = express.Router();
const moment = require('moment');

//getting organizations
router.get('/organizationsCount', authenticate, async(req, res) => {
    try {
        const count = await Organization.count();
        res.json(count)
    }
    catch (error) {
        res.status(404)
        .send(error);
    }
})

//getting departments
router.get('/departmentsCount', authenticate, async(req, res) => {
    try {
        const count = await Department.count();
        res.json(count)
    }
    catch (error) {
        res.status(404)
        .send(error);
    }
})

//getting employees
router.get('/employeesCount', authenticate, async(req, res) => {
    try {
        const count = await Employee.count();
        res.json(count)
    }
    catch (error) {
        res.status(404)
        .send(error);
    }
})

//getting 1M chart data
router.get('/get1MChartData/:dateFrom/:dateTo', async(req, res) => {
    try {   
        //get the date from params
        const date = moment(req.params.dateFrom);

        //number of days in the selected month
        const daysOfMonth = date.endOf('month').format('DD');
        
        //get respective grouped-by, ordered-by, counted data
        //ogs = organizations
        //dps = departments
        //emps = employees
        const ogs = await getGroupedData(Organization, date, 'DATE', 'month');
        const dps = await getGroupedData(Department, date, 'DATE', 'month');
        const emps = await getGroupedData(Employee ,date, 'DATE', 'month');

        const data = {
            'labels' : daysOfMonth,
            'selectedMonth': date.month(),
            'selectedYear': date.year(),
            'ogs' : createDataArray(ogs, daysOfMonth),
            'dps' : createDataArray(dps, daysOfMonth),
            'emps' : createDataArray(emps, daysOfMonth),
        }
        res.json(data);
        // res.json(ogs)
    } 
    catch (error) {
        console.log(error)
        res.status(500)
        .send(error);
    }
})
router.get('/get1YChartData/:dateFrom/:dateTo', async(req, res) => {
    try {   
        const date = moment(req.params.dateFrom);
        
        //12 months in a year
        const monthsOfYear = 12;
        
        //get respective grouped-by, ordered-by, counted data
        //ogs = organizations
        //dps = departments
        //emps = employees
        const ogs = await getGroupedData(Organization, date, 'MONTH', 'year');
        const dps = await getGroupedData(Department, date, 'MONTH', 'year');
        const emps = await getGroupedData(Employee ,date, 'MONTH', 'year');

        const data = {
            'labels' : monthsOfYear,
            'selectedYear' : date.year(),
            'ogs' : createDataArray(ogs, monthsOfYear),
            'dps' : createDataArray(dps, monthsOfYear),
            'emps' : createDataArray(emps, monthsOfYear),
        }
        res.json(data);
        // res.json(ogs)
    } 
    catch (error) {
        res.status(500)
        .send(error);
    }
})

//unit - DATE OR MONTH to perform sql truncate
//range  start of 'month' or 'year'
//groupby and count
const getGroupedData = (entity, date, unit, range) => { 
    return entity.findAll({
    raw: true,
    attributes: [
        [Sequelize.fn(`${unit}`, Sequelize.col('timestamps')), 'createdOn'],
        [Sequelize.literal(`COUNT(*)`), 'count']
    ],
    where: {
        timestamps: {
            [Op.gte]: date.startOf(`${range}`).format('YYYY-MM-DD'),
            [Op.lte]: date.endOf(`${range}`).format('YYYY-MM-DD'),
        }
    },
    group: 'createdOn',
    order: [[Sequelize.literal('createdOn'), "ASC"]]
    })
}

//results : data retrieved from database 
//n : numbers of days or months
const createDataArray = (results, n) => {
    //create array with the size of the month's days
    let _arr = new Array(parseInt(n)).fill(0);

    results.forEach(x => {
        let day;
        if(n === 12) {
            day = new Date(x.createdOn);
        }
        else {
            day = new Date(x.createdOn).getDate();
        }
        _arr[day-1] = x.count;
    })
    return _arr;
}

module.exports = router;

