const sequelize = require('../db.config.js');
const { DataTypes, Model } = require('sequelize');
const Department = require('./department');
const Employee = require('./employee');

class Organization extends Model {}

Organization.init({
    name : {
        type: DataTypes.STRING,
        allowNull: false
    },
    owner : {
        type: DataTypes.STRING,
        allowNull: false
    },
    address : {
        type: DataTypes.STRING,
        allowNull: false
    },
    city : {
        type: DataTypes.STRING,
        allowNull: false
    },
    state : {
        type: DataTypes.STRING,
        allowNull: false
    },
    country : {
        type: DataTypes.STRING,
        allowNull: false
    },
    timestamps : {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, {
    sequelize,
    timestamps: false,
    modelName: 'Organization'
});
Department.belongsTo(Organization, {
    foreignKey: {
        allowNull: false
    },
    onDelete: 'CASCADE'
});
Employee.belongsTo(Department, {
    foreignKey: {
        allowNull: false
    },
    onDelete: 'CASCADE'
});
Organization.hasMany(Department);
Department.hasMany(Employee);

module.exports = Organization;