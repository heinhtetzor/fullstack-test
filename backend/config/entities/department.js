const sequelize = require('../db.config.js');
const { DataTypes, Model } = require('sequelize');

class Department extends Model {}

Department.init({
    owner : {
        type: DataTypes.STRING,
        allowNull: false
    },
    description : {
        type: DataTypes.STRING,
        allowNull: false
    },
    workingtime : {
        type: DataTypes.STRING,
        allowNull: false
    },
    workingdays : {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    timestamps : {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, {
    sequelize,
    timestamps: false,
    modelName: 'Department'
});


module.exports = Department;