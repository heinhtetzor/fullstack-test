const sequelize = require('../db.config.js');
const { DataTypes, Model } = require('sequelize');

class Employee extends Model {}

Employee.init({
    firstName : {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName : {
        type: DataTypes.STRING,
        allowNull: false
    },
    dob : {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    workTitle : {
        type: DataTypes.STRING,
        allowNull: false
    },
    totalExperience : {
        type: DataTypes.STRING,
        allowNull: false
    },
    timestamps : {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, {
    sequelize,
    timestamps: false,
    modelName: 'Employee'
});

module.exports = Employee;