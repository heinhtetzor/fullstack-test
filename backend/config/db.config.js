const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('fullstacktest', 'root', '12345', {
    dialect : 'mysql',
    host : 'database',
    port : 3306,
    logging: false
})
// sequelize.sync();
sequelize.sync({ force: true });



module.exports = sequelize;